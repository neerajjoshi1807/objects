function cou(x,y)
{
    if(x==0)
    {
        return(1);
    }
    var arr= [];
    for(var k=0;k<x+1;k++)
    {
        arr[k]=0;
    }
    arr[0]=1;
    for(var i=1;i<=x;i++)
    {
        var tot=0;
        for(var j of y)
        {
            if(i-j>=0)
            {
                tot+=arr[i-j];
            }
        }
        arr[i]=tot;
    }
    return(arr[x]);
}


function variables(object)
{
    var x=object.steps;
    var y=object.ability;
    return(cou(x,y));
}
module.exports=variables;
